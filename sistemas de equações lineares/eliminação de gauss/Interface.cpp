#include "Interface.h"
#include <cmath>
#include <iostream>
using namespace std;

void trocarLinhas(double** A, double* b, int j, int k, int n){
    int i;
    double t;

    for( i=0; i<n; i++){
        t = A[j][i];
        A[j][i] = A[k][i];
        A[k][i] = t;
    }

    t = b[j];
    b[j] = b[k];
    b[k] = t;
}

void pivotacaoParcial(double** A, double* b, int k, int n){
    int i, j;
    double m;

    j = k;
    m = abs( A[k][k] );

    for( i=k+1; i<n; i++){
        if( m < abs( A[i][k] ) ){
            m = abs( A[i][k] );
            j = i;
        }
    }

    if( j != k )
        trocarLinhas(A, b, j, k, n);
}

void eliminacaoDeGaussComPivotacaoParcial(double** A, double* b, int n){
    double m;
    int i, j, k;

    for(k=0; k<n-1; k++){
    	cout << "Etapa " << k+1 << " antes do pivoteamento" << endl;
        mostrarMatriz(A, b, n);
        pivotacaoParcial(A, b, k, n);
        cout << "Etapa " << k+1 << " depois do pivoteamento" << endl;
        mostrarMatriz(A, b, n);
        for(i=k+1; i<n; i++){
            m = - A[i][k]/A[k][k];
            cout << "Eliminando elemento a" << i+1 << "," << k+1 << " = " << A[i][k] << " com fator " << m << endl;
            mostrarMatriz(A, b, n);
            A[i][k] = 0;
            for(j=k+1; j<n; j++)
                A[i][j] = A[i][j] + m*A[k][j];
            b[i] = b[i] + m*b[k];
        }
    }
    cout << "Fim" << endl;
    mostrarMatriz(A, b, n);
}

double* metodoDeGaussComPivotacaoParcial(double** A, double* b, int n){
    eliminacaoDeGaussComPivotacaoParcial(A, b, n);
    return resolverTriangularSuperior(A, b, n);
}

double* resolverTriangularSuperior(double** A, double* b, int n){
    double* x = new double[n];
    float s;
    int k, j;

    x[n-1] = b[n-1]/A[n-1][n-1];
    for(k=n-2; k>-1; k--){
        s = 0;
        for(j=k+1; j<n; j++)
            s = s + A[k][j]*x[j];
        x[k] = (b[k] - s)/A[k][k];
    }

    return x;
}

void eliminacaoDeGauss(double** A, double* b, int n){
    double m;
    int i, j, k;

    for(k=0; k<n-1; k++){
    	cout << "Etapa " << k+1 << endl;
        for(i=k+1; i<n; i++){
            m = - A[i][k]/A[k][k];
            cout << "Eliminando elemento a" << i+1 << "," << k+1 << " = " << A[i][k] << " com fator " << m << endl;
            mostrarMatriz(A, b, n);
            A[i][k] = 0;
            for(j=k+1; j<n; j++)
                A[i][j] = A[i][j] + m*A[k][j];
            b[i] = b[i] + m*b[k];
        }
    }
    cout << "Fim" << endl;
    mostrarMatriz(A, b, n);
}

double* metodoDeGauss(double** A, double* b, int n){
    eliminacaoDeGauss(A, b, n);
    return resolverTriangularSuperior(A, b, n);
}

void mostrarMatriz(double** A, double* b, int n){
	int i, j;
	
	for(i=0; i<n; i++){
		for(j=0; j<n; j++)
			cout << A[i][j] << " ";
		cout << b[i] << endl;		
	}
	cout << endl;
}

#ifndef INTERFACE_H
#define INTERFACE_H

void trocarLinhas(double**, double*, int, int, int);
void pivotacaoParcial(double**, double*, int, int);
void eliminacaoDeGaussComPivotacaoParcial(double**, double*, int);
double* metodoDeGaussComPivotacaoParcial(double**, double*, int);

double* resolverTriangularSuperior(double**, double*, int);
void eliminacaoDeGauss(double**, double*, int);
double* metodoDeGauss(double**, double*, int);

void mostrarMatriz(double** A, double* b, int n);

#endif // INTERFACE_H

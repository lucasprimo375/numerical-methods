#include <iostream>
#include "Interface.h"
#include <iomanip>
using namespace std;

void receberMatriz(double**, int);
void receberVetor(double*, int);
void mostrarVetor(double*, int);
void copiarMatriz(double**, double**, int);
void copiarVetor(double*, double*, int);

int main(){
	cout << fixed << setprecision(6) << endl;
	
    int n;
    cout << "Entre a quantidade de incógnitas: ";
    cin >> n;

    double** A = new double*[n];
    double** B = new double*[n];
    receberMatriz(A, n);
    copiarMatriz(A, B, n);

    double* b = new double[n];
    double* c = new double[n];
    receberVetor(b, n);
    copiarVetor(b, c, n);

    double* x;
    double* y;
    
    cout << endl;
    cout << "MÉTODO DE GAUSS SEM PIVOTEAMENTO:" << endl;
    x = metodoDeGauss(A, b, n);
    mostrarVetor(x, n);
    
    cout << endl;
    
    cout << "MÉTODO DE GAUSS COM PIVOTEAMENTO PARCIAL:" << endl;
    y = metodoDeGaussComPivotacaoParcial(B, c, n);
    mostrarVetor(y, n);
}

void copiarVetor(double* entr, double* copia, int n){
	for( int i=0; i<n; i++)
		copia[i] = entr[i];
}

void copiarMatriz(double** entrada, double** copia, int n){
	int i, j;
	
	for( i=0; i<n; i++ ){
		copia[i] = new double[n];
		for( j=0; j<n; j++ )
			copia[i][j] = entrada[i][j];
	}
}

void mostrarVetor(double* v, int n){
    int i;

    cout << "Mostrando o vetor." << endl;
    for(i=0; i<n; i++)
        cout << "x[" << i+1 << "] = " << v[i] << endl;
}

void receberVetor(double* b, int n){
    int i;

    cout << "Entre os índices do vetor b." << endl;
    for(i=0; i<n; i++){
        cout << "Entre o índice da linha " << i+1 << ": ";
        cin >> b[i];
    }
}

void receberMatriz(double** A, int n){
    int i, j;

    cout << "Entre os índices da matriz." << endl;
    for(i=0; i<n; i++){
        A[i] = new double[n];
        for(j=0; j<n; j++){
            cout << "Entre o indice da linha " << i+1 << " e da coluna " << j+1 << ": ";
            cin >> A[i][j];
        }
    }
}

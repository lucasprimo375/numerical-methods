#include "Interface.h"
#include <cmath>
#include <iostream>
using namespace std;

double* metodoLUComPivotacaoParcial(double** A, double* b, int n){
	double** L = fatoracaoLUComPivotacaoParcial(A, b, n);
	double* y = resolverTriangularInferior(L, b, n);
	return resolverTriangularSuperior(A, y, n);
}

double* metodoLU(double** A, double* b, int n){
	double** L = fatoracaoLU(A, n);
	double* y = resolverTriangularInferior(L, b, n);
	return resolverTriangularSuperior(A, y, n);
}

double** fatoracaoLUComPivotacaoParcial(double** A, double* b, int n){
	double** L = new double*[n];
	double m;
    int i, j, k;

	carregarIdentidade(L, n);

    for(k=0; k<n-1; k++){
    	pivotacaoParcial(A, b, k, n);
        for(i=k+1; i<n; i++){
            m = - A[i][k]/A[k][k];
            L[i][k] = -m;
            A[i][k] = 0;
            for(j=k+1; j<n; j++)
                A[i][j] = A[i][j] + m*A[k][j];
        }
    }
   
	return L;
}


double** fatoracaoLU(double** A, int n){
	double** L = new double*[n];
	double m;
    int i, j, k;

	carregarIdentidade(L, n);

    for(k=0; k<n-1; k++){
        for(i=k+1; i<n; i++){
            m = - A[i][k]/A[k][k];
            L[i][k] = -m;
            A[i][k] = 0;
            for(j=k+1; j<n; j++)
                A[i][j] = A[i][j] + m*A[k][j];
        }
    }
	
	return L;
}

double* resolverTriangularInferior(double** A, double* b, int n){
	double* x = new double[n];
    float s;
    int k, j;

    x[0] = b[0]/A[0][0];
    for(k=1; k<n; k++){
        s = 0;
        for(j=0; j<k; j++)
            s = s + A[k][j]*x[j];
        x[k] = (b[k] - s)/A[k][k];
    }

    return x;
}

double* resolverTriangularSuperior(double** A, double* b, int n){
	double* x = new double[n];
    float s;
    int k, j;

    x[n-1] = b[n-1]/A[n-1][n-1];
    for(k=n-2; k>-1; k--){
        s = 0;
        for(j=k+1; j<n; j++)
            s = s + A[k][j]*x[j];
        x[k] = (b[k] - s)/A[k][k];
    }

    return x;
}

void carregarIdentidade(double** L, int n){
	int i, j;
	
	for( i=0; i<n; i++ ){
		L[i] = new double[n];
		for( j=0; j<n; j++ ){
			if( i == j )
				L[i][j] = 1;
			else
				L[i][j] = 0;
		}
	}
}

void trocarLinhas(double** A, double* b, int j, int k, int n){
    int i;
    double t;

    for( i=k; i<n; i++){
        t = A[j][i];
        A[j][i] = A[k][i];
        A[k][i] = t;
    }
    
    t = b[j];
    b[j] = b[k];
    b[k] = t;
}

void pivotacaoParcial(double** A, double* b, int k, int n){
    int i, j;
    double m;

    j = k;
    m = abs( A[k][k] );

    for( i=k+1; i<n; i++){
        if( m < abs( A[i][k] ) ){
            m = abs( A[i][k] );
            j = i;
        }
    }

    if( j != k )
        trocarLinhas(A, b, j, k, n);
}

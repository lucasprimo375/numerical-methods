#ifndef INTERFACE_H
#define INTERFACE_H

double* metodoLUComPivotacaoParcial(double** A, double* b, int n);

double* metodoLU(double** A, double* b, int n);

double** fatoracaoLUComPivotacaoParcial(double** A, double* b, int n);

double** fatoracaoLU(double** A, int n);

double* resolverTriangularInferior(double** A, double* b, int n);

double* resolverTriangularSuperior(double** A, double* b, int n);

void carregarIdentidade(double** L, int n);

void trocarLinhas(double** A, double* b, int j, int k, int n);

void pivotacaoParcial(double** A, double* b, int k, int n);

#endif // INTERFACE_H

#ifndef JACOBI_H
#define JACOBI_H

double* somaVetorial(double* u, double* v, int n);
double** gerarMatrizDeIteracao(double** A, int n);
double* gerarVetorAuxiliar(double** A, double* b, int n);
double* metodoDeGaussJacobi(double** A, double* b, int n, double erro, int maxIter);

#endif

#include "OperacoesBasicas.h"
#include "Seidel.h"
#include <iostream>
using namespace std;

double* gerarProximaAproximacao(double* aproxAtual, double** A, double* b, int n){
	int i, j;
	double s;
	double* proxAprox = new double[n];
	
	s = 0;
	for( i=1; i<n; i++ )
		s = s + (-A[0][i])*aproxAtual[i];
	proxAprox[0] = (s + b[0])/A[0][0];
	
	for( i=1; i<n; i++ ){
		s = 0;
		for( j=0; j<i; j++ ){
			if( i != j )
				s = s + (-A[i][j])*proxAprox[j];
		}
		for( j=i; j<n; j++ ){
			if( i != j )
				s = s + (-A[i][j])*aproxAtual[j];
		}
		
		proxAprox[i] = (s + b[i])/A[i][i];
	}
	
	return proxAprox;
}

double* metodoDeGaussSeidel(double** A, double* b, int n, double erro, int maxIter){
	int k;
	double m;
	double* x;
	double* y;
	
	x = gerarChuteInicial(A, b, n);
	k = 0;
	do{
		cout << "Iteração " << k << endl;
		mostrarVetor(x, n);
		
		y = gerarProximaAproximacao(x, A, b, n);
		
		m = erroRelativo(y, x, n);
		
		x = y;
		
		k++;
	}while( ( m>erro ) && ( k<maxIter ) );
	
	cout << "Iteração " << k << endl << "Solução final" << endl;
	mostrarVetor(x, n);
	
	return x;
}

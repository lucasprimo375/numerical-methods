#include "OperacoesBasicas.h"
#include <iostream>
#include <cmath>
using namespace std;

void mostrarVetor(double* v, int n){
	int i;

	for( i=0; i<n; i++ )
		cout << "x[" << i+1 << "] = " << v[i] << endl;
}

void receberVetor(double* b, int n){
	int i;

	cout << "Entre os elementos do vetor solução." << endl;
	for( i=0; i<n; i++ ){
		cout << "Entre o elementos da posição " << i+1 << " : ";
		cin >> b[i];
	}
}

void receberMatriz(double** A, int n){
	int i, j;

	cout << "Entre os elementos da matriz de coeficientes" << endl;
	for( i=0; i<n; i++ ){
		A[i] = new double[n];
		for( j=0; j<n; j++ ){
			cout << "Entre o elemento da linha " << i+1 << " e da coluna " << j+1 << " : ";
			cin >> A[i][j];
		}
	}
}

double max(double* v, int n){
	double m = abs( v[0] );
	int i;

	for( i=1; i<n; i++ ){
		if( abs(v[i]) > m )
			m = abs( v[i] );
	}

	return m;
}

double erroRelativo(double* u, double *v, int n){
	double erroAbsoluto, maximo;
	int i;

	erroAbsoluto = abs(u[0] - v[0]);
	for( i=1; i<n; i++ ){
		if( abs(u[i] - v[i]) > erroAbsoluto )
			erroAbsoluto = abs(u[i] - v[i]);
	}

	maximo = max(u, n);

	return erroAbsoluto/maximo;
}

double* gerarChuteInicial(double** A, double* b, int n){
	double* x = new double[n];
	int i;

	for( i=0; i<n; i++ )
		x[i] = b[i]/A[i][i];

	return x;
}

double* multiplicarVetorPorMatriz(double** A, double* v, int n){
	double* w = new double[n];
	int i, j;
	double s;

	for( i=0; i<n; i++ ){
		s = 0;
		for( j=0; j<n; j++ )
			s = s + A[i][j]*v[j];
		w[i] = s;
	}

	return w;
}

void verificarConvergencia(double** A, double* b, double* x, int n, double erro){
	double* y;
	double m;

	y = multiplicarVetorPorMatriz(A, x, n);

	m = erroRelativo(y, b, n);

	if( m <= erro )
		cout << "O método convergiu" << endl;
	else
		cout << "O método possivelmente divergiu" << endl;
}

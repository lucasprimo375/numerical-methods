#ifndef BASICAS_H
#define BASICAS_H

// Mostrar as componentes de um vetor no terminal.
void mostrarVetor(double* v, int n);

// Recebe do usuário as componentes do vetor.
void receberVetor(double* b, int n);

// Recebe do usuário os coeficientes que multiplicam as variáveis.
void receberMatriz(double** A, int n);

// Retorna o elemento máximo do vetor.
double max(double* v, int n);

// Calcula a maior diferença entre componentes dos vetores u e v,
// isso seria o erro absoluto.
// O erro relativo é a razão entre a maior componente do vetor u e o
// erro absulto.
double erroRelativo(double* u, double *v, int);

// Gera o chute inicial para esse sistema.
double* gerarChuteInicial(double** A, double* b, int n);

double* multiplicarVetorPorMatriz(double** A, double* v, int n);

// Essa função substitui o vetor solução no sistema linear e verifica
// se essa solução é válida, isto é, se o erro relativo entre o produto 
// da matriz de coeficientes pelo vetor solução é menor que o erro
// estabelecido pelo usuário.
void verificarConvergencia(double** A, double* b, double* x, int n, double erro);

#endif

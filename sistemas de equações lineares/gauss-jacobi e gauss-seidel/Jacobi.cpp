#include "OperacoesBasicas.h"
#include "Jacobi.h"
#include <iostream>
using namespace std;

double* somaVetorial(double* u, double* v, int n){
	double* w = new double[n];
	int i;

	for( i=0; i<n; i++ )
		w[i] = u[i] + v[i];

	return w;
}

double** gerarMatrizDeIteracao(double** A, int n){
	double** C = new double*[n];
	int i, j;

	for( i=0; i<n; i++ ){
		C[i] = new double[n];
		for( j=0; j<n; j++ ){
			if( i == j )
				C[i][j] = 0;
			else
				C[i][j] = -A[i][j]/A[i][i];
		}
	}

	return C;
}

double* gerarVetorAuxiliar(double** A, double* b, int n){
	double* g = new double[n];
	int i;

	for( i=0; i<n; i++ ){
		g[i] = b[i]/A[i][i];
	}

	return g;
}

double* metodoDeGaussJacobi(double** A, double* b, int n, double erro, int maxIter){
	double* x;
	double* y;
	double* g;
	double** C;
	double m;
	int k;

	C = gerarMatrizDeIteracao(A, n);
	g = gerarVetorAuxiliar(A, b, n);

	x = gerarChuteInicial(A, b, n);

	k = 0;
	do{
		cout << "Iteração " << k << endl;
		mostrarVetor(x, n);

		y = multiplicarVetorPorMatriz(C, x, n);
		y = somaVetorial(y, g, n);

		m = erroRelativo(y, x, n);

		x = y;

		k++;
	}
	while( ( m>erro ) && ( k<maxIter ) );
	
	cout << "Iteração " << k << endl << "Solução final" << endl;
	mostrarVetor(x, n);

	return x;
}

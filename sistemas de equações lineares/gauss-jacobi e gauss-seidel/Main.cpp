#include "OperacoesBasicas.h"
#include "Jacobi.h"
#include "Seidel.h"
#include <iostream>
#include <iomanip>
using namespace std;

int main(){
	cout << fixed << setprecision(6);

	int n;

	cout << "Entre a quantidade de variáveis: ";
	cin >> n;

	double erro;
	cout << "Entre a precisão: ";
	cin >> erro;

	int maxIter;
	cout << "Entre o número de iterações máximo: ";
	cin >> maxIter;

	double** A = new double*[n];
	receberMatriz(A, n);

	double* b = new double[n];
	receberVetor(b, n);

	double* x;

	cout << "MÉTODO DE GAUSS-JACOBI" << endl;
	x = metodoDeGaussJacobi(A, b, n, erro, maxIter);
	verificarConvergencia(A, b, x, n, erro);
	
	cout << endl;
	
	cout << "MÉTODO DE GAUSS-SEIDEL" << endl;
	x = metodoDeGaussSeidel(A, b, n, erro, maxIter);
	verificarConvergencia(A, b, x, n, erro);
}

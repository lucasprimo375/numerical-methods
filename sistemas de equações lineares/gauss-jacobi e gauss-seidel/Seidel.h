#ifndef SEIDEL_H
#define SEIDEL_H

double* gerarProximaAproximacao(double* aproxAtual, double** A, double* b, int n);
double* metodoDeGaussSeidel(double** A, double* b, int n, double erro, int maxIter);

#endif

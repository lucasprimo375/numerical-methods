#include <iostream>
#include <iomanip>
#include "Interface.h"
using namespace std;

void receberMatriz(double** A, int n);
void copiarMatriz(double** entrada, double** copia, int n);

int main(){
	cout << fixed << setprecision(6);

    int n;
    cout << "Entre a dimensão da matriz: ";
    cin >> n;

    double** A = new double*[n];
    double** B = new double*[n];
    receberMatriz(A, n);
    copiarMatriz(A, B, n);
    
    cout << "O determinante da matriz entrada é: " << determinante(A, n) << endl << endl;
    
    cout << "A matriz inversa da matriz entrada é: " << endl;
    A = matrizInversa(B, n);
    mostrarMatriz(A, n);
    
    cout << endl;
}

void copiarMatriz(double** entrada, double** copia, int n){
	int i, j;
	
	for( i=0; i<n; i++ ){
		copia[i] = new double[n];
		for( j=0; j<n; j++ )
			copia[i][j] = entrada[i][j];
	}
}

void receberMatriz(double** A, int n){
    int i, j;

    cout << "Entre os índices da matriz." << endl;
    for(i=0; i<n; i++){
        A[i] = new double[n];
        for(j=0; j<n; j++){
            cout << "Entre o indice da linha " << i+1 << " e da coluna " << j+1 << ": ";
            cin >> A[i][j];
        }
    }
}

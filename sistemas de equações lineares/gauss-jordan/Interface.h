#ifndef INTERFACE_H
#define INTERFACE_H

double** matrizInversa(double** A, int n);
double** eliminacaoDeGaussJordan(double** A, int n);
void pivotacaoParcial(double** A, double** I, int k, int n);
void trocarLinhas(double** A, double** I, int j, int k, int n);
void carregarIdentidade(double** L, int n);

double determinante(double** A, int n);
int eliminacaoDeGauss(double** A, int n);

// t é o número de vezes que houve troca de linha.
// Se houver troca de linha nesse método, t é incrementado
// em uma unidade. E retornamos o novo valor de t;
int pivotacaoParcial(double** A, int k, int t, int n);

void trocarLinhas(double** A, int j, int k, int n);

void mostrarMatriz(double** A, int n);

#endif // INTERFACE_H

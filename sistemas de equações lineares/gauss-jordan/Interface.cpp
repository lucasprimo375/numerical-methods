#include "Interface.h"
#include <cmath>
#include <iostream>
using namespace std;

double** matrizInversa(double** A, int n){
	double** I;
	int i, j;
	
	I = eliminacaoDeGaussJordan(A, n);
	
	for( i=0; i<n; i++ ){
		for( j=0; j<n; j++ ){
			I[i][j] = I[i][j]/A[i][i];
		}
		
		A[i][i] = 1;
	}
	
	return I;
}

double** eliminacaoDeGaussJordan(double** A, int n){
	double m;
    int i, j, k;
	double** I = new double*[n];
	
	carregarIdentidade(I, n);

    for(k=0; k<n; k++){
    	pivotacaoParcial(A, I, k, n);
        for(i=k+1; i<n; i++){
            m = - A[i][k]/A[k][k];
            A[i][k] = 0;
            for(j=k+1; j<n; j++)
                A[i][j] = A[i][j] + m*A[k][j];
            
            for(j=0; j<n; j++)
                I[i][j] = I[i][j] + m*I[k][j];
        }
        
        for(i=k-1; i>-1; i--){
            m = - A[i][k]/A[k][k];
            A[i][k] = 0;
            for(j=0; j<n; j++)
                I[i][j] = I[i][j] + m*I[k][j];
            
            for(j=k+1; j<n; j++)
                A[i][j] = A[i][j] + m*A[k][j];
        }
    }
    
    return I;
}

void pivotacaoParcial(double** A, double** I, int k, int n){
	int i, j;
    double m;

    j = k;
    m = abs( A[k][k] );

    for( i=k+1; i<n; i++){
        if( m < abs( A[i][k] ) ){
            m = abs( A[i][k] );
            j = i;
        }
    }

    if( j != k )
        trocarLinhas(A, I, j, k, n);
}

void trocarLinhas(double** A, double** I, int j, int k, int n){
	int i;
	double t;
	
	for( i=0; i<n; i++ ){
		t = A[j][i];
		A[j][i] = A[k][i];
		A[k][i] = t;
		
		t = I[j][i];
		I[j][i] = I[k][i];
		I[k][i] = t;
	}
}

void carregarIdentidade(double** L, int n){
	int i, j;
	
	for( i=0; i<n; i++ ){
		L[i] = new double[n];
		for( j=0; j<n; j++ ){
			if( i == j )
				L[i][j] = 1;
			else
				L[i][j] = 0;
		}
	}
}

double determinante(double** A, int n){
	int t, i;
	double d;
	
	t = eliminacaoDeGauss(A, n);
	
	d = 1;
	for( i=0; i<n; i++ )
		d = d*A[i][i];
		
	if( t % 2 == 0 )
		return d;
		
	return -d;
}

int eliminacaoDeGauss(double** A, int n){
	double m;
    int i, j, k, t;

	t = 0;
    for(k=0; k<n-1; k++){
    	t = pivotacaoParcial(A, k, t, n);
        for(i=k+1; i<n; i++){
            m = - A[i][k]/A[k][k];
            A[i][k] = 0;
            for(j=k+1; j<n; j++)
                A[i][j] = A[i][j] + m*A[k][j];
        }
    }
   
	return t;
}

int pivotacaoParcial(double** A, int k, int t, int n){
    int i, j;
    double m;

    j = k;
    m = abs( A[k][k] );

    for( i=k+1; i<n; i++){
        if( m < abs( A[i][k] ) ){
            m = abs( A[i][k] );
            j = i;
        }
    }

    if( j != k ){
        trocarLinhas(A, j, k, n);
        t++;
    }
    
    return t;
}

void trocarLinhas(double** A, int j, int k, int n){
    int i;
    double t;

    for( i=k; i<n; i++){
        t = A[j][i];
        A[j][i] = A[k][i];
        A[k][i] = t;
    }
}

void mostrarMatriz(double** A, int n){
	int i, j;
	
	for(i=0; i<n; i++){
		for(j=0; j<n; j++)
			cout << A[i][j] << " ";
		cout << endl;		
	}
	cout << endl;
}

#ifndef INTERFACE_H
#define INTERFACE_H

void falsePosition(double a, double b, double precision);
void bissection(double a, double b, double precision);
double f(double x);
void promptUserInteraction();

#endif // INTERFACE_H

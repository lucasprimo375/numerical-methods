#include "Interface.h"
#include <iostream>
using namespace std;

int main(){
	double precision = 0.00001;

	cout << "Bissection Method" << endl;
	cout << "Function (-0.5)*(e^x) + 2*cos(x)" << endl;
	cout << "Initial Interval: [0,1]" << endl;
	cout << "Precision: " << precision << endl;
	bissection(0, 1, precision);
	
	cout << endl;
	
	promptUserInteraction();
	
	cout << "False Position Method" << endl;
	cout << "Function (-0.5)*(e^x) + 2*cos(x)" << endl;
	cout << "Initial Interval: [0,1]" << endl;
	cout << "Precision: " << precision << endl;
	falsePosition(0, 1, precision);
}

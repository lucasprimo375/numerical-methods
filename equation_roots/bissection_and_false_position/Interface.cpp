#include <cmath>
#include <iostream>
#include <iomanip>
#include "Interface.h"
using namespace std;

void falsePosition(double a, double b, double precision){
    double x;
    int k=1;

    cout << fixed << setprecision(6);

    do{
        promptUserInteraction();
        x = ( a*f(b) - b*f(a) )/( f(b) - f(a) );

        cout << "Iteration " << k << endl;
        cout << "a = " << a << " => f(a) = " << f(a) << endl;
        cout << "b = " << b << " => f(b) = " << f(b) << endl;
        cout << "x = " << x << " => f(x) = " << f(x) << endl;

        if( f(x)*f(a) <= 0 ){
            b = x;
            cout << "The root is between " << a << " and " << x << endl;
        }
        else/*( f(x)*f(b) <= 0 )*/{
            a = x;
            cout << "The root is between " << x << " and " << b << endl;
        }

        k++;
    }
    while( abs(b-a)>precision && abs(f(x))>precision );

    cout << "After " << k << " iteractions, the aproximate root is " << x << endl;
}

void bissection(double a, double b, double precision){
    double x;
    int k=1;

    cout << fixed << setprecision(6);

    do{
        promptUserInteraction();
        x = (a+b)/2;

        cout << "Iteration " << k << endl;
        cout << "a = " << a << " => f(a) = " << f(a) << endl;
        cout << "b = " << b << " => f(b) = " << f(b) << endl;
        cout << "x = " << x << " => f(x) = " << f(x) << endl;

        if( f(x)*f(a) <= 0 ){
            b = x;
            cout << "The root is between " << a << " and " << x << endl;
        }
        else/*( f(x)*f(b) <= 0 )*/{
            a = x;
            cout << "The root is between " << x << " and " << b << endl;
        }

        k++;
    }
    while( abs(b-a)>precision && abs(f(x))>precision );

    cout << "After " << k << " iteractions, the aproximate root is " << x << endl;
}

double f(double x){
    return exp(x)*(-0.5) + 2*cos(x);
}

void promptUserInteraction(){
    do{
        cout << "Press any key to continue" << endl;
    }while(cin.get() != '\n');
}

#include "Interface.h"
#include <iostream>
using namespace std;

int main(){
	double A[4] = {3, -9, 0, 1};
	double Q[4] = {0.33333, 0, 0, 0.11111};

	int degree = 3;
	int iteration_polynomial_degree = 3;
	double initial_guess = 0.25;
	double precision = 0.0001;
	
	cout << "ROOT FOR THE POLYNOMIAL METHOD: " << polynomial_method(A, degree, initial_guess, precision) << endl;
	
	cout << "ROOT FOR THE FIXED POINT METHOD: " << fixed_point(A, Q, degree, iteration_polynomial_degree, initial_guess, precision) << endl;
}

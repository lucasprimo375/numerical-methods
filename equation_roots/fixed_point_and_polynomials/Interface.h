#ifndef INTERFACE_H
#define INTERFACE_H

// A is a vector containing the polynomial coefficients that we want to calculate the root,
// Q is a vector containing the coefficients of the iteration polynomial, n is A's degree,
// m is Q's degree and x0 is the initial guess.
double fixed_point(double* A, double* Q, int n, int m, double x0, double precision);

// A is a vector containing the polynomial coefficients that we want to calculate the root,
// n is its degree and x0 is the initial guess.
// Returns the polynomial root.
double polynomial_method(double* A, int n, double x0, double precision);

// A is a vector containing the polynomial coefficients that we want to calculate
// the derivative e n is its degree.
// Returns a vector containing the derivative's coefficients. 
double* derivative(double* A, int n);

// A is a vector containing the polynomial coefficients, n is the polynomial degree
// e c is the point at which we want to calculate.
double calculate_value(double* A, int n, double c);

#endif

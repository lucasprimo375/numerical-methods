#include "Interface.h"
#include <cmath>
#include <iostream>
#include <iomanip>
using namespace std;

double fixed_point(double* A, double* Q, int n, int m, double x0, double precision){
	double x1, f;
	int k;
	
	cout << fixed << setprecision(6) << endl;
	
	k = 1;
	double old_x0;
	
	cout << "RUNNING THE FIXED POINT METHOD" << endl;
	do {
		old_x0 = x0;
		x1 = calculate_value(Q, m, x0);
		f = calculate_value(A, n, x1);
		x0 = x1;

		double old_x0_value = calculate_value(A, n, old_x0);
		cout << "ITERATION " << k << endl;
		cout << "X0 = " << old_x0 << " => f(X0) = " << old_x0_value << endl;
		cout << "X1 = " << x1 << " => f(X1) = " << f << endl;
	
		k++;
	} while((abs(x1-old_x0) > precision) && (abs(f) > precision));
	
	cout << endl;
	
	return x1;
}

double polynomial_method(double* A, int n, double x0, double precision){
	double* derivative_coefficients;
	double x1, f;
	int k;
	
	cout << fixed << setprecision(6) << endl;
	
	derivative_coefficients = derivative(A, n);

	k = 1;
	double old_x0;

	cout << "RUNNING THE POLYNOMIAL METHOD" << endl;
	do {
		old_x0 = x0;
		x1 = x0 - calculate_value(A, n, x0)/(calculate_value(derivative_coefficients, n-1, x0));
		f = calculate_value(A, n, x1);
		x0 = x1;

		cout << "ITERATION " << k << endl;
		cout << "X" << k << " = " << old_x0 << " => f(X0) = " << calculate_value( A, n, old_x0 ) << endl;
		cout << "X" << k+1 << " = " << x1 << " => f(X" << k+1 << ") = " << f << endl;
	
		k++;
	} while((abs(x1-old_x0) > precision) && (abs(f) > precision));
	
	cout << endl;
	
	return x1;
}

double* derivative(double* A, int n) {
	double* derivative_coefficients = new double[n];
	
	for(int i=1; i<=n; i++)
		derivative_coefficients[i-1] = A[i]*i;
		
	return derivative_coefficients;
}

double calculate_value(double* A, int n, double c) {
	double value;
	
	value = A[n];
	for (int i=n-1; i>=0; i--)
		value = A[i] + value*c;
		
	return value;
} 

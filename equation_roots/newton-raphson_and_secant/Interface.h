#ifndef INTERFACE_H
#define INTERFACE_H

void secant(double x, double x1, double precision);
void newton_raphson(double x, double precision);
double g(double x); // g calculates f's derivative
double f(double x);
void promptUserInteraction();

#endif

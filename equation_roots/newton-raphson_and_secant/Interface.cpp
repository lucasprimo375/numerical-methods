#include "Interface.h"
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

void secant(double x, double x1, double precision){
	double x2;
	int k=1;
	
	x2 = ( x*f(x1) - x1*f(x) )/( f(x1) - f(x) );
	while( abs(x2-x1)>precision && abs(f(x2))>precision ){
		promptUserInteraction();
		cout << "Iteration " << k << endl;
		cout << "X" << k-1 << " = " << x << " => f(X" << k-1 << ") = " << f(x) << endl;
		cout << "X" << k << " = " << x1 << " => f(X" << k << ") = " << f(x1) << endl;
		cout << "X" << k+1 << " = " << x2 << " => f(X" << k+1 << ") = " << f(x2) << endl;
		
		x = x1;
		
		x1 = x2;
		
		x2 = ( x*f(x1) - x1*f(x) )/( f(x1) - f(x) );
		
		k++;
	}
	
	cout << "After " << k-1 << " iterations, the approximate root will be " << x2 << endl;
}

void newton_raphson(double x, double precision){
	double x1;
	int k=1;
	
	x1 = x - f(x)/g(x);
	while( abs(x1-x)>precision && abs(f(x1))>precision ){
		promptUserInteraction();
		cout << "Iteration " << k << endl;
		cout << "X" << k-1 << " = " << x << " => f(X" << k-1 << ") = " << f(x) << endl;
		cout << "X" << k << " = " << x1 << " => f(X" << k << ") = " << f(x1) << endl;
		
		x = x1;
		
		x1 = x - f(x)/g(x);
		
		k++;
	}
	
	cout << "After " << k-1 << " iterations, the approximate root will be " << x2 << endl;
}

double g(double x){
	return exp(x)*(-0.5) - 2*sin(x);	
} 

double f(double x){
    return exp(x)*(-0.5) + 2*cos(x);
}

void promptUserInteraction(){
    do{
        cout << "Press any key to continue" << endl;
    }while(cin.get() != '\n');
}


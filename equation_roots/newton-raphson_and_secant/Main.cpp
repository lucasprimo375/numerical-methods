#include "Interface.h"
#include <iostream>
using namespace std;

int main(){
	cout << fixed << setprecision(6);

	cout << "Método de Newton-Raphson" << endl;
	newton_raphson(0.5, 0.0001);
	
	cout << endl;

	promptUserInteraction();
	
	cout << "Método da Secante" << endl;
	secant(0.5, 1, 0.0001);
}

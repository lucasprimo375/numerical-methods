#ifndef INTERFACE_H
#define INTERFACE_H

void fixed_point(double x, double precision);
void newton_raphson(double x, double precision);
double g(double x); // g calculates f's derivative
double f(double x);
double q(double x); // iteration function
void promptUserInteraction();

#endif

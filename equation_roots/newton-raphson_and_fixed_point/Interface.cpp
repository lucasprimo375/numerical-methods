#include "Interface.h"
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

void fixed_point(double x, double precision){
	double x1;
	int k=1;
		
	x1 = q(x);
	
	promptUserInteraction();

	cout << "Iteration " << k << endl;
	cout << "X" << k-1 << " = " << x << " => f(X" << k-1 << ") = " << f(x) << endl;
	cout << "X" << k << " = " << x1 << " => f(X" << k << ") = " << f(x1) << endl;
	
	while( abs(x1-x)>precision && abs(f(x1))>precision ){
		promptUserInteraction();

		k++;
		x = x1;
		
		x1 = q(x);
	
		cout << "Iteration " << k << endl;
		cout << "X" << k-1 << " = " << x << " => f(X" << k-1 << ") = " << f(x) << endl;
		cout << "X" << k << " = " << x1 << " => f(X" << k << ") = " << f(x1) << endl;
	}
	
	cout << "After " << k-1 << " iterations, the aproximate root will be " << x1 << ", with value " << f(x1) << endl;
	
}

void newton_raphson(double x, double precision){
	double x1;
	int k=1;
		
	x1 = x - f(x)/g(x);

	promptUserInteraction();
	
	cout << "Iteration " << k << endl;
	cout << "X" << k-1 << " = " << x << " => f(X" << k-1 << ") = " << f(x) << endl;
	cout << "X" << k << " = " << x1 << " => f(X" << k << ") = " << f(x1) << endl;
	
	while( abs(x1-x)>precision && abs(f(x1))>precision ){
		promptUserInteraction();

		k++;
		x = x1;
		
		x1 = x - f(x)/g(x);
	
		cout << "Iteration " << k << endl;
		cout << "X" << k-1 << " = " << x << " => f(X" << k-1 << ") = " << f(x) << endl;
		cout << "X" << k << " = " << x1 << " => f(X" << k << ") = " << f(x1) << endl;
	}
	
	cout << "After " << k-1 << " iterations, the aproximate root will be " << x1 << ", with value " << f(x1) << endl;
}

double q(double x){
	return x/log(x);
}

double g(double x){ 
	return -log(x);	
} 

double f(double x){
    return x - x*log(x);
}

void promptUserInteraction(){
    do{
        cout << "Press any key to continue" << endl;
    }while(cin.get() != '\n');
}

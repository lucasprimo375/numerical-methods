#include "Interface.h"
#include <iostream>
#include <iomanip>

using namespace std;

int main(){
	cout << fixed << setprecision(6);

	double precision = 0.00001;
	double initial_guess = 2.7;
	cout << "Newton-Raphson Method" << endl;
	cout << "Function: x - x*ln(x)" << endl;
	cout << "Initial Guess: " << initial_guess << endl;
	cout << "Precision: " << precision << endl;
	newton_raphson(initial_guess, precision);
	
	cout << endl;
	
	promptUserInteraction();

	cout << "Fixed Point Method" << endl;
	cout << "Function: x - x*ln(x)" << endl;
	cout << "Initial Guess: " << initial_guess << endl;
	cout << "Precision: " << precision << endl;
	fixed_point(initial_guess, precision);
}

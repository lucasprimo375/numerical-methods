#ifndef INTERFACE_H
#define INTERFACE_H

#include <string>

/* Função que recebe um número inteiro num e o converte para
   binário.
*/
std::string decimalParaBinario(int num);

/* Função que recebe um inteiro bin, que representa o número em
   binário, e o converte para um número
   decimal inteiro.
*/
int binarioParaDecimalDefault(int bin);

/* Função que converte um número binário para decimal
   utilizando o método dos parenteses. Ela recebe um inteiro bin,
   que representa o número em binário, e retorna sua conversão para binário.
*/
int binarioParaDecimalParenteses(int bin);

#endif // INTERFACE_H

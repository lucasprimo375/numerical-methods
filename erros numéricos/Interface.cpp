#include "Interface.h"
#include <cmath>
#include <cmath>
#include <iostream>
using namespace std;

string decimalParaBinario(int num){
    string A = "";

    int i = (int)ceil(log2(num)) - 1;
    while( num > 0  ){

        if( num % 2 == 0 )
            A = A + '0';
        else
            A = A + '1';
        num = floor(num/2);
        i--;
    }

    return A;
}

int binarioParaDecimalDefault(int bin){
    int soma = 0;
	int k=0;
    while( bin > 0 ){
    	int i = bin % 10;
    	soma = soma + i*pow(2, k);
    	k++;
    	bin = bin/10;
    }   

    return soma;
}

int binarioParaDecimalParenteses(int bin){
    if( bin == 0 )
    	return 0;
	
	return bin % 10 + 2*binarioParaDecimalParenteses(bin/10);
}

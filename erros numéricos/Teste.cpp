#include <iostream>
#include "Interface.h"
using namespace std;

int menu();
string inverte(string, int);

int main(){
    string entrada;
    int num;

    int c = menu();

    switch(c){
        case 1:
            cout << "Entre o inteiro: " << endl;
            cin >> num;
            entrada = decimalParaBinario(num);
            cout << num << " em binário é " << inverte(entrada, entrada.size()) << endl;
            break;
        case 2:
        	cout << "Entre a string binária: " << endl;
        	cin >> num;
        	cout << binarioParaDecimalDefault(num) << endl;
        	break;
        case 3:
        	cout << "Entre a string binária: " << endl;
        	cin >> num;
        	cout << binarioParaDecimalParenteses(num) << endl;
        	break;
    }

}

int menu(){
    int c = 0;

    while( c!=1 && c!=2 && c!=3 ){
        cout << "Escolha uma das opções: " << endl;
        cout << "1: Conversão decimal para binário." << endl;
        cout << "2: Conversão binário para decimal pelo método padrão." << endl;
        cout << "3: Conversão binário para decimal pelo método dos parentêses." << endl;

        cin >> c;
    }

    return c;
}

string inverte(string s, int n){
	string ss = "";
	
	for(int i=n-1;i>=0;i--)
		ss = ss + s[i];
		
	return ss;
}

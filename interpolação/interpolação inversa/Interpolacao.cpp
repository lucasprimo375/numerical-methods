#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

double calcularL(double* X, double* F, int n, int k, double x);
double lagrange(double* X, double* F, int n, double x);

double** tabelarDiferencasDivididas(double* X, double* F, int n);
double* tabelarProdutos(double* X, double x, int n);
double newton(double* X, double** A, double x, int n);

void receberVetor(double* X, double* F, int n);

int main(){
    int n;
    double* X, *F;
    double y;

    cout << "Entre a quantidade de pontos: ";
    cin >> n;
    n--;

    X = new double[n+1];
    F = new double[n+1];

    receberVetor(X, F, n);

    double** A = tabelarDiferencasDivididas(F, X, n);

    cout << fixed << setprecision(6);
    while( true ){
        cout << "Interpolação Inversa" << endl;
        cout << "Entre um valor a ser calculado ( ctrl+c para sair ): ";
        cin >> y;

        cout << "Interpolação por Newton: f(" << y << ") = " << newton(F, A, y, n) << endl;

        cout << "Interpolação por Lagrange: f(" << y << ") = " << lagrange(F, X, n, y) << endl;
    }
}

double newton(double* X, double** A, double x, int n){
    double* L = tabelarProdutos(X, x, n);
    int i;
    double s;

    s = A[0][0];
    for( i=0; i<n; i++ )
        s = s + L[i]*A[0][i+1];

    return s;
}

double* tabelarProdutos(double* X, double x, int n){
    int i;
    double* L = new double[n];

    L[0] = x - X[0];
    for( i=1; i<n; i++ )
        L[i] = L[i-1]*( x - X[i] );

    return L;
}

double** tabelarDiferencasDivididas(double* X, double* F, int n){
    double** A = new double*[n+1];
    int i, j, l;

    for( i=0; i<=n; i++ ){
        A[i] = new double[n+1];
        A[i][i] = F[i];
    }

    for( l=1; l<=n; l++ ){
        for( i=0; i<=n-l; i++ ){
            j = i + l;
            A[i][j] = ( A[i][j-1] - A[i+1][j] )/( X[i] - X[j] );
        }
    }

    return A;
}

double calcularL(double* X, double* F, int n, int k, double x){
    double p, q;
    int j;

    p = 1;
    q = 1;
    for( j=0; j<=n; j++ ){
        if( j != k ){
            p = p*( x - X[j] );
            q = q*( X[k] - X[j] );
        }
    }

    return p/q;
}

double lagrange(double* X, double* F, int n, double x){
    double s;
    int k;

    s = 0;
    for( k=0; k<=n; k++ )
        s = s + F[k]*calcularL(X, F, n, k, x);

    return s;
}

void receberVetor(double* X, double* F, int n){
    int i;

    cout << "Entre os Xi e os f(Xi)" << endl;
    for( i=0; i<=n; i++ ){
        cout << "Entre o X" << i << ": ";
        cin >> X[i];

        cout << "Entre o f(X" << i << "): ";
        cin >> F[i];
    }
}

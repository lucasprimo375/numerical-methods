#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

double calcularL(double* X, double* F, int n, int k, double x);
double lagrange(double* X, double* F, int n, double x);

double** tabelarDiferencasDivididas(double* X, double* F, int n);
double* tabelarProdutos(double* X, double x, int n);
double newton(double* X, double** A, double x, int n);

double* metodoDeGaussComPivotacaoParcial(double** A, double* b, int n);
void eliminacaoDeGaussComPivotacaoParcial(double** A, double* b, int n);
void pivotacaoParcial(double** A, double* b, int k, int n);
void trocarLinhas(double** A, double* b, int j, int k, int n);
double* resolverTriangularSuperior(double** A, double* b, int n);
void montarSistema(double** A, double* b, double* X, double* F, int n);
double calcularValor(double* A, int n, double c);

void receberVetor(double* X, double* F, int n);

int main(){
    int n;
    double* X, *F;
    double x;

    cout << "Entre a quantidade de pontos: ";
    cin >> n;
    n--;

    X = new double[n+1];
    F = new double[n+1];

    receberVetor(X, F, n);

    double** A = tabelarDiferencasDivididas(X, F, n);

    double** B = new double*[n+1];
    //for( int i=0; i<=n; i++ ){B[i] = new double[n+1];}
    double* b = new double[n+1];
    montarSistema(B, b, X, F, n);
    double* C = metodoDeGaussComPivotacaoParcial(B, b, n);

    cout << fixed << setprecision(6);
    while( true ){
        cout << "Entre um valor a ser calculado ( ctrl+c para sair ): ";
        cin >> x;

        cout << "Interpolação pelo Sistema Linear: f(" << x << ") = " << calcularValor(C, n, x) << endl;

        cout << "Interpolação por Newton: f(" << x << ") = " << newton(X, A, x, n) << endl;

        cout << "Interpolação por Lagrange: f(" << x << ") = " << lagrange(X, F, n, x) << endl;
    }
}

void montarSistema(double** A, double* b, double* X, double* F, int n){
    int i, j;

    /*b = new double[n+1];
    A = new double*[n+1];*/
    for( i=0; i<=n; i++ ){
        A[i] = new double[n+1];
    }

    for( i=0; i<=n; i++ ){
        b[i] = F[i];
    }

    for( i=0; i<=n; i++ ){
        for( j=0; j<=n; j++ ){
            A[i][j] = pow( X[i], j );
        }
    }
}

void trocarLinhas(double** A, double* b, int j, int k, int n){
    int i;
    double t;

    for( i=0; i<=n; i++){
        t = A[j][i];
        A[j][i] = A[k][i];
        A[k][i] = t;
    }

    t = b[j];
    b[j] = b[k];
    b[k] = t;
}

void pivotacaoParcial(double** A, double* b, int k, int n){
    int i, j;
    double m;

    j = k;
    m = abs( A[k][k] );

    for( i=k+1; i<=n; i++){
        if( m < abs( A[i][k] ) ){
            m = abs( A[i][k] );
            j = i;
        }
    }

    if( j != k )
        trocarLinhas(A, b, j, k, n);
}

void eliminacaoDeGaussComPivotacaoParcial(double** A, double* b, int n){
    double m;
    int i, j, k;

    for(k=0; k<n; k++){
        pivotacaoParcial(A, b, k, n);
        for(i=k+1; i<=n; i++){
            cout << A[i][k] << " " << A[k][k] << endl;
            m = - A[i][k]/A[k][k];
            A[i][k] = 0;
            for(j=k+1; j<=n; j++)
                A[i][j] = A[i][j] + m*A[k][j];
            b[i] = b[i] + m*b[k];
        }
    }
}

double* metodoDeGaussComPivotacaoParcial(double** A, double* b, int n){
    eliminacaoDeGaussComPivotacaoParcial(A, b, n);
    return resolverTriangularSuperior(A, b, n);
}

double* resolverTriangularSuperior(double** A, double* b, int n){
    double* x = new double[n];
    float s;
    int k, j;

    x[n-1] = b[n-1]/A[n-1][n-1];
    for(k=n-2; k>-1; k--){
        s = 0;
        for(j=k+1; j<n; j++)
            s = s + A[k][j]*x[j];
        x[k] = (b[k] - s)/A[k][k];
    }

    return x;
}

double calcularValor(double* A, int n, double c){
	double b;
	int i;

	b = A[n];
	for( i=n-1; i>=0; i-- )
		b = A[i] + b*c;

	return b;
}

double newton(double* X, double** A, double x, int n){
    double* L = tabelarProdutos(X, x, n);
    int i;
    double s;

    s = A[0][0];
    for( i=0; i<n; i++ )
        s = s + L[i]*A[0][i+1];

    return s;
}

double* tabelarProdutos(double* X, double x, int n){
    int i;
    double* L = new double[n];

    L[0] = x - X[0];
    for( i=1; i<n; i++ )
        L[i] = L[i-1]*( x - X[i] );

    return L;
}

double** tabelarDiferencasDivididas(double* X, double* F, int n){
    double** A = new double*[n+1];
    int i, j, l;

    for( i=0; i<=n; i++ ){
        A[i] = new double[n+1];
        A[i][i] = F[i];
    }

    for( l=1; l<=n; l++ ){
        for( i=0; i<=n-l; i++ ){
            j = i + l;
            A[i][j] = ( A[i][j-1] - A[i+1][j] )/( X[i] - X[j] );
        }
    }

    return A;
}

double calcularL(double* X, double* F, int n, int k, double x){
    double p, q;
    int j;

    p = 1;
    q = 1;
    for( j=0; j<=n; j++ ){
        if( j != k ){
            p = p*( x - X[j] );
            q = q*( X[k] - X[j] );
        }
    }

    return p/q;
}

double lagrange(double* X, double* F, int n, double x){
    double s;
    int k;

    s = 0;
    for( k=0; k<=n; k++ )
        s = s + F[k]*calcularL(X, F, n, k, x);

    return s;
}

void receberVetor(double* X, double* F, int n){
    int i;

    cout << "Entre os Xi e os f(Xi)" << endl;
    for( i=0; i<=n; i++ ){
        cout << "Entre o X" << i << ": ";
        cin >> X[i];

        cout << "Entre o f(X" << i << "): ";
        cin >> F[i];
    }
}
